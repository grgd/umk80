
var app = {
	state: "open", // open, closed

	dom: {
		modelView: $("#model-view"),
		btnShutter: $("#btn-shutter"),
		btnPlay: $("#btn-play"),
		btnReset: $('#btn-reset'),
		iconShutter: $("#btn-shutter").find("span")
	},

	switchMode: function () {
		switch(app.state) {
			case "open":
				app.dom.iconShutter.
					removeClass("glyphicon-arrow-up").
					addClass("glyphicon-arrow-down");
				app.dom.modelView[0].style.maxHeight = "0px";
				app.state = "closed";
				break;
			case "closed":
				app.dom.iconShutter.
					removeClass("glyphicon-arrow-down").
					addClass("glyphicon-arrow-up");
				app.dom.modelView[0].style.maxHeight = window.innerWidth * 0.75 + "px";
				app.state = "open";
				break;
		}
	},

	startStopSimulation: function() {
		if(modelView.running()) {
			modelView.stop();
			app.dom.btnPlay.text("Play");
			app.dom.btnPlay.removeClass("btn-info");
		} else {
			modelView.start();
			if(app.state == "closed")
				app.switchMode();
			app.dom.btnPlay.text("Pause");
			app.dom.btnPlay.addClass("btn-info");
		}
	},

	resetSimulation: function() {
		modelView.reset();
		app.dom.btnPlay.text("Play");
		app.dom.btnPlay.removeClass("btn-info");
		modelView.repaint();
	},

	init: function () {
		
		this.dom.btnShutter.click(app.switchMode);
		this.dom.btnPlay.click(app.startStopSimulation);
		this.dom.btnReset.click(app.resetSimulation);
		this.switchMode();

		//modelView.repaint(true);
	}
};

app.init();