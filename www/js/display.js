
function toHEX (number) {
	return number.toString(16);
}

function fromHEX (hex) {
	return parseInt(hex, 16);
}

/**
 * Represents an IO buffer with LCD screen
 * @param {int} size - length of buffer
 * @param {DOMElement} span - element which holds the text
 */
var Display = function(size, span) {
	this.size = size;
	this.value = 0;
	this.string = '';
	this.span = span;
	this.max = size == 2 ? 256 : 65536;
	this.default = this.string = size == 2 ? '..' : '....';
	this.empty = true;
}

Display.prototype.setValue = function(value) {
	if(value != undefined && value != null) {
		value = value % this.max;
		this.value = value;
	} else {
		value = this.value;
	}

	this.empty = false;

	var text = toHEX(value);
	while(text.length < this.size)
		text = '0' + text;
	this.render(text);

	//console.log(this.value);
};

Display.prototype.addDigit = function(digit) {
	this.value *= 16;
	this.value += digit;
	this.value = this.value % this.max;

	this.empty = false;

	this.string += toHEX(digit);
	var l = this.string.length;
	this.render(this.string.substr(l - this.size));
};

Display.prototype.reset = function(text) {
	this.value = 0;
	this.empty = true;
	if(text) {
		this.render(text);
	}
	else
		this.render(this.default);
};

Display.prototype.setText = function(text) {
	this.render(text);
};

Display.prototype.jam = function() {
	this.span.innerText = this.default;
};

Display.prototype.render = function(text) {
	this.span.innerText = this.default;
	var self = this;
	if(text)
		this.string = text;
	self.span.innerText = self.string.toUpperCase();
};