	var UMK = UMK || {};

	UMK.modes = {};
	UMK.modes.idle = {
		name: 'idle',

		init: function() {
			UI.resetDisplay('-...', '..')
		},

		onData: function(data) {
			UMK.changeState('confuse');
		},

		onMode: function(mode) {
			if(mode === 'space' || mode === 'execute')
				UMK.changeState('confuse');
			switch(mode) {
				case 'memory':
						UMK.changeState('memory1');
					break;
				case 'register':
						UMK.changeState('register1');
					break;
				case 'checksum':
						UMK.changeState('checksum1');
					break;
				case 'constfill':
						UMK.changeState('constfill1');
					break;
				case 'arraymove':
						UMK.changeState('arraymove1');
					break;
				case 'start':
					UMK.changeState('start');
					break;
			}
		}
	};

	UMK.modes.confuse = {
		name: 'confuse',

		init: function() {
			UI.resetDisplay('-...', '.?');
		},

		onData: function(data) {
			UMK.changeState('confuse');
		},

		onMode: function(mode) {
			if(mode === 'space' || mode === 'execute') {
				UMK.changeState('confuse');
				return;
			}
			switch(mode) {
				case 'memory':
						UMK.changeState('memory1');
					break;
				case 'register':
						UMK.changeState('register1');
					break;
				case 'checksum':
						UMK.changeState('checksum1');
					break;
				case 'constfill':
						UMK.changeState('constfill1');
					break;
				case 'arraymove':
						UMK.changeState('arraymove1');
					break;
				case 'start':
						UMK.changeState('start');
					break;
			}
		}
	};

	// ввод адреса первой ячейки
	UMK.modes.memory1 = {
		name: 'memory1', 

		init: function() {
			UI.resetDisplay('....', '..');
		},

		onData: function(data) {
			UI.displayLeft.addDigit(data);
		},

		onMode: function(mode) {
			if(UI.displayLeft.empty) {
				UMK.changeState('confuse');
				return;
			}
			if(mode === 'space' || mode === 'execute') {
				UMK.changeState('memory2');
				return;
			}
			UMK.changeState('confuse');
		}
	};

	// последующее шляние по памяти
	UMK.modes.memory2 = {
		name: 'memory2',
		currentCell: -1,

		init: function() {
			this.currentCell = UI.displayLeft.value;
			//console.log("Current: " + UMK.getRAM(this.currentCell))
			UI.displayRight.setValue( UMK.getRAM(this.currentCell) );
			UI.displayLeft.render();
		},

		onData: function(data) {
			UI.displayRight.addDigit(data);
		}, 

		onMode: function(mode) {
			if(mode === 'space') {
				UMK.setRAM(this.currentCell, UI.displayRight.value);
				this.currentCell++;
				UI.displayLeft.setValue(this.currentCell);
				UI.displayRight.setValue( UMK.getRAM(this.currentCell) );
			} else {
				UMK.changeState('confuse');
			}
		}
	};

	// ввод номера регистра
	UMK.modes.register1 = {
		name: 'register1',

		init: function() {
			UI.resetDisplay('....', '..');
		},

		onData: function(data) {
			if(data < 4 || data > 15) {
				UI.disableRegisterMode();
				UMK.changeState('confuse');
				return;
			}
			UI.displayLeft.value = data;
			UI.displayLeft.setText(UMK.registerNames[data] + '-');
			UMK.changeState('register2');
		}, 

		onMode: function(mode) {
			UMK.changeState('confuse');
		}
	};

	// ввод значения регистра
	UMK.modes.register2 = {
		name: 'register2',
		currentRegister: 0,

		init: function() {
			this.currentRegister = UI.displayLeft.value;
			UI.displayRight.setValue( UMK.getRegister(this.currentRegister) );
		},

		onData: function(data) {
			UI.displayRight.addDigit(data);
		}, 

		onMode: function(mode) {
			if(mode === 'space') {
				UMK.setRegister( this.currentRegister, UI.displayRight.value );
				UMK.changeState('register1');
			} else {
				UMK.changeState('confuse');
			}
		}
	};

	UMK.modes.checksum1 = {
		name: 'checksum1',
		from: -1,
		to: -1,
		step: 0,

		init: function() {
			UI.resetDisplay('....', '..');
			this.step = 0;
		},

		onData: function(data) {
			if(this.step < 2)
				UI.displayLeft.addDigit(data);
			else
				UMK.changeState('confuse');
		},

		onMode: function(mode) {
			if(this.step < 2 && (mode === 'space' || mode === 'execute')) {
				if(UI.displayLeft.empty) {
					UMK.changeState('confuse');
					return;
				}
				if(this.step == 0) {
					this.step = 1;
					this.from = UI.displayLeft.value;
					UI.resetDisplay('....', '..');
					return;
				}
				if(this.step == 1) {
					this.step = 2;
					this.to = UI.displayLeft.value;
					if(this.to < this.from) {
						UMK.changeState('confuse');
						return;
					}
					UI.displayLeft.reset('-...');
					UI.displayRight.setValue( this.calcChecksum() );
					return;
				}
			}
			if(this.step == 2) {
				if(mode == 'checksum') {
					this.init();
				} else {
					UMK.changeState('confuse');
					return;
				}
			}
		},

		calcChecksum: function() {
			var s = 0;
			for(var i=this.from; i<=this.to; i++) {
				s += UMK.getRAM(i);
			}
			return s % 256;
		}
	};

	UMK.modes.constfill1 = {
		name: 'constfill1',

		from: -1,
		to: -1,
		constant: 0,
		step: 0,

		init: function() {
			this.step = 0;
			UI.resetDisplay('....', '..');
		},

		onData: function(data) {
			if(this.step < 2) {
				UI.displayLeft.addDigit(data);
			} else {
				UI.displayRight.addDigit(data);
			}
		},

		onMode: function(mode) {
			switch(this.step) {
				case 0:
					if(mode == 'space' && !UI.displayLeft.empty) {
						this.step = 1;
						this.from = UI.displayLeft.value;
						UI.resetDisplay('....', '..');
					} else {
						UMK.changeState('confuse');
					}
					break;
				case 1:
					if(mode == 'space' && !UI.displayLeft.empty) {
						this.step = 2;
						this.to = UI.displayLeft.value;
						UI.displayRight.reset('..');
					} else {
						UMK.changeState('confuse');
					}
					break;
				case 2:
					if(mode == 'space' || mode == 'execute' && (!UI.displayRight.empty)) {
						this.constant = UI.displayRight.value;
						this.fill();
						UMK.changeState('idle');
					} else {
						UMK.changeState('confuse');
					}
					break;
			}
		},

		fill: function() {
			for(var i=this.from; i<=this.to; i++)
				UMK.setRAM(i, this.constant);
		}
	};

	UMK.modes.arraymove1 = {
		name: 'arraymove1',

		from: -1,
		to: -1,
		destination: 0,
		step: 0,

		init: function() {
			this.step = 0;
			UI.resetDisplay('....', '..');
		},

		onData: function(data) {
			UI.displayLeft.addDigit(data);
		},

		onMode: function(mode) {
			switch(this.step) {
				case 0:
					if(mode == 'space' && !UI.displayLeft.empty) {
						this.step = 1;
						this.from = UI.displayLeft.value;
						UI.resetDisplay('....', '..');
					} else {
						UMK.changeState('confuse');
					}
					break;
				case 1:
					if(mode == 'space' && !UI.displayLeft.empty) {
						this.step = 2;
						this.to = UI.displayLeft.value;
						UI.displayLeft.reset('..');
					} else {
						UMK.changeState('confuse');
					}
					break;
				case 2:
					if(mode == 'space' || mode == 'execute' && (!UI.displayLeft.empty)) {
						this.destination = UI.displayLeft.value;
						this.move();
						UMK.changeState('idle');
					} else {
						UMK.changeState('confuse');
					}
					break;
			}
		},

		move: function() {
			for(var i=this.from; i<=this.to; i++) {
				var m = UMK.getRAM(i);
				UMK.setRAM(this.destination + i, m);
			}
		}
	};

	UMK.modes.start = {
		name: 'start',

		from: -1,
		to: -1,

		step: 0,

		init: function() {
			this.step = 0;
			UI.resetDisplay('....', '..');
		},

		onData: function(data) {
			UI.displayLeft.addDigit(data);
		},

		onMode: function(mode) {
			switch(this.step) {
				case 0:
					if(mode == 'space' && !UI.displayLeft.empty) {
						this.step = 1;
						this.from = UI.displayLeft.value;
						UI.resetDisplay('...-', '..');
					} else {
						UMK.changeState('confuse');
					}
					break;
				case 1:
					if(mode == 'execute' && !UI.displayLeft.empty) {
						this.step = 2;
						this.to = UI.displayLeft.value;

						if(this.from > this.to) {
							this.changeState('idle');
							return;
						}
						UI.resetDisplay('....', '..');
						UMK.runCode(this.from, this.to);
						UMK.changeState('idle');
					} else {
						UMK.changeState('confuse');
					}
					break;
				// здесь мог быть еще третий аргумент, но он нужен только для режима отладки УМК.
			}
		}
	};