	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};

	var touchDevice = (isMobile.any()) ? true : false,
		msTouchDevice = window.navigator.msMaxTouchPoints ? true : false;

	function addPushListener(element, handler) {
		if(msTouchDevice)
			element.addEventListener("pointerdown", handler);
		else if(touchDevice)
			element.addEventListener("touchstart", handler);
		else
			element.addEventListener("mousedown", handler);
	}

	function addReleaseListener(element, handler) {
		if(msTouchDevice)
			element.addEventListener("pointerup", handler);
		else if(touchDevice)
			element.addEventListener("touchend", handler);
		else
			element.addEventListener("mouseup", handler);
	}


	var UMK = UMK || {};
	UMK.mode = 'idle';
	UMK.RAM = new Uint8Array(65536);
	for(var i=0; i<UMK.RAM.length; i++)
		UMK.RAM[i] = i % 256;
	UMK.registers = {
		a: 170, b: 187, c: 204, d: 221, e: 238, f: 255, h: 18, l: 52, ph: 0, pl: 64, sh: 15, sl: 188
	};
	UMK.flags = {
		z: 0, s: 0, p: 0, c: 0, ca: 0
	};
	UMK.registerNames = {
		4: 'PCL',
		5: 'PCH',
		6: 'SPH',
		7: 'SPL',
		8: '..H',
		9: '..L',
		10: '..A',
		11: '..B',
		12: '..C',
		13: '..D',
		14: '..E',
		15: '..F'
	};
	UMK.registerIDs = {
		4: 'pl',
		5: 'ph',
		6: 'sh',
		7: 'sl',
		8: 'h',
		9: 'l',
		10: 'a',
		11: 'b',
		12: 'c',
		13: 'd',
		14: 'e',
		15: 'f',
		16: 'm'
	};
	UMK.registerCodes = {
		7: 'a',
		0: 'b',
		1: 'c',
		2: 'd',
		3: 'e',
		4: 'h',
		5: 'l',
		6: 'm'
	};
	UMK.registerPairCodes = {
		'bc': 0,
		'de': 1,
		'hl': 2,
		's': 3
	};
	UMK.flagCodes = {
		's': 0,
		'z': 1,
		'ac': 2,
		'p': 3,
		'c': 4
	};

	UMK.getRegister = function(id) {
		if(typeof(id) === 'string') {
			return UMK.registers[id];
		}
		if(typeof(id) === 'number') {
			return UMK.registers[ UMK.registerIDs[id] ];
		}
	}

	UMK.setRegister = function(id, value) {
		while(value < 0)
			value += 256;
		value = value % 256;
		if(typeof(id) === 'string') {
			UMK.registers[id] = value;
		}
		if(typeof(id === 'number')) {
			UMK.registers[ UMK.registerIDs[id] ] = value;
		}
	}

	UMK.getRegisterPair = function(id) {
		var hi, lo;
		switch(id) {
			case 'b':
				hi = 'b'; lo = 'c';
				break;
			case 'd':
				hi = 'd'; lo = 'e';
				break;
			case 'h':
				hi = 'h'; lo = 'l';
				break;
			case 's':
				hi = 'sh'; lo = 'sl';
				break;
		}
		var result = UMK.getRegister(lo);
		result += UMK.getRegister(hi) * 256;
		return result;
	}

	UMK.getFlag = function(id) {
		return (UMK.registers.f >> UMK.flagCodes[id]) & 1;
	}

	UMK.setFlag = function(id, value) {
		var mask = 1 << UMK.flagCodes[id];
		if(value)
			UMK.registers.f = UMK.registers.f | mask;
		else 	
			UMK.registers.f = UMK.registers.f & (~mask);
	}

	UMK.getRAM = function(index) {
		if(index < 0 || index >= UMK.RAM.length)
			return 256;
		return UMK.RAM[index];
	}

	UMK.setRAM = function(index, value) {
		if(index < 2048 || index >= UMK.RAM.length)
			return;
		while(value < 0)
			value += 256;
		value = value % 256;
		UMK.RAM[index] = value;
	}

	UMK.changeState = function(newState) {
		UMK.currentState = UMK.modes[newState];
		UMK.currentState.init();
	}

	var UI = {
		modeButtons: {},
		dataButtons: [],
		displayLeft: null,
		displayRight: null,
		registerMode: false,

		init: function() {
			var mb = $('button[id*="mode"]');
			for(var i=0; i<mb.length; i++) {
				var name = mb[i].id.substr("mode-".length);
				UI.modeButtons[name] = i;
				addPushListener(mb[i], UI.jam);

				addReleaseListener(mb[i], UI.modePress);
				mb[i].setAttribute('data-mode', name);
			}

			for(var i=0; i<16; i++) {
				var db = $('#data-' + toHEX(i))[0];
				db.setAttribute('data-data', i);
				addPushListener(db, UI.jam);

				addReleaseListener(db, UI.dataPress);
				UI.dataButtons.push( db );
			}

			UI.displayRight = new Display(2, $('#display-right')[0]);
			UI.displayLeft = new Display(4, $('#display-left')[0]);

			UI.resetDisplay();
		},

		jam: function(event) {
			UI.displayLeft.jam();
			UI.displayRight.jam();
		},

		dataPress: function(event) {
			var digit = parseInt(event.currentTarget.getAttribute('data-data'));
			UMK.currentState.onData(digit);
		},

		modePress: function(event) {
			var mode = event.currentTarget.getAttribute('data-mode');
			UMK.currentState.onMode(mode);
		},

		resetDisplay: function(leftText, rightText) {
			UI.displayLeft.reset(leftText);
			UI.displayRight.reset(rightText);
		},

		enableRegisterMode: function() {
			if(this.registerMode)
				return;
			this.registerMode = true;
			for(var i=4; i<16; i++) {
				UI.dataButtons[i].innerText = UMK.registerIDs[i].toUpperCase();
			}
		},

		disableRegisterMode: function() {
			if(!this.registerMode)
				return;
			this.registerMode = false;
			for(var i=4; i<16; i++) {
				UI.dataButtons[i].innerText = toHEX(i);
			}
		}
	};

	UI.init();
	UMK.changeState('idle');