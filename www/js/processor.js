(function processor () {
	var UMK = window.UMK || {};

	UMK.runCode = function(from, to) {
		buffer.op = from;
		var counter = 0;
		while(buffer.op != to && buffer.op < 65536 && counter < 2048) {
			processOpCode(UMK.getRAM(buffer.op));
			counter++;
		}
	};

	var buffer = {
		op: 0,
		source: 0,
		dest: 0,
		memory: 0
	};

	function countBits(val) {
		var r = 0;
		for(var i = 0; i < 8; i++) {
			r += val & 1;
			val = val >> 1;
		}
		return r;
	}

	var opcodes = [
		{
			name: "MOV r1,r2",
			template: "01DDDSSS",
			size: 1,
			operate: function() {
				var sid = UMK.registerCodes[buffer.source],
					did = UMK.registerCodes[buffer.dest];
				console.log(sid + ' ' + did);
				UMK.setRegister(did, UMK.getRegister(sid));
				buffer.op++;
			}
		},
		{
			name: "MOV M,r",
			template: "01110SSS",
			size: 1,
			operate: function() {
				var h = UMK.getRegisterPair('h'),
					val = UMK.getRegister(UMK.registerCodes[buffer.source]);
				console.log(h + ' ' + val);
				UMK.setRAM(h, val);
				buffer.op++;
			}
		},
		{
			name: "MVI r,data",
			template: "00DDD110",
			size: 2,
			operate: function() {
				buffer.op++;
				var data = UMK.getRAM(buffer.op),
					did = UMK.registerCodes[buffer.dest];
				console.log(did + ' ' + data);
				UMK.setRegister(did, data);
				buffer.op++;
			}
		},
		{
			name: "ANI data",
			template: "11100110",
			size: 2,
			operate: function() {
				buffer.op++;
				var data = UMK.getRAM(buffer.op),
					a = UMK.getRegister('a');
				console.log(data + ' ' + a);
				UMK.setRegister('a', a & data);
				buffer.op++;
			}
		},
		{
			name: "J condition addr",
			template: "11CCC010",
			size: 3,
			operate: function() {
				var addr = 0,
					flag = 0;
				buffer.op++;
				addr = UMK.getRAM(buffer.op);
				buffer.op++;
				addr += UMK.getRAM(buffer.op) * 256;
				console.log(buffer.cond + ' ' + addr);
				switch(buffer.cond) {
					case 0:
						flag = !UMK.getFlag('z');
						break;
					case 1:
						flag = UMK.getFlag('z');
						break;
					case 2:
						flag = !UMK.getFlag('c');
						break;
					case 3:
						flag = UMK.getFlag('c');
						break;
					case 4:
						flag = !UMK.getFlag('p');
						break;
					case 5:
						flag = UMK.getFlag('p');
						break;
					case 6:
						flag = !UMK.getFlag('s');
						break;
					case 7:
						flag = UMK.getFlag('c');
						break;
				}
				if(flag)
					buffer.op = addr;
				else
					buffer.op++;
			}
		},
		{
			name: "JMP addr",
			template: "11000011",
			size: 3,
			operate: function() {
				var addr = 0;
				buffer.op++;
				addr = UMK.getRAM(buffer.op);
				console.log(addr);
				buffer.op++;
				addr += UMK.getRAM(buffer.op) * 256;
				buffer.op = addr;
			}
		},
		{
			name: "SUB M",
			template: "10010110",
			size: 1,
			operate: function() {
				var m = UMK.getRegisterPair('h'),
					a = UMK.getRegister('a'),
					r = a - m;
				UMK.setFlag('s', r < 0 ? 1 : 0);
				UMK.setFlag('z', r == 0 ? 1 : 0);
				UMK.setFlag('c', 0);
				if(r < 0) r += 256;
				UMK.setFlag('p', countBits(r) % 2 == 0 ? 1 : 0);
				UMK.setRegister('a', r);
				buffer.op++;
			}
		},
		{
			name: "SUB r",
			template: "10010SSS",
			size: 1,
			operate: function() {
				var a = UMK.getRegister('a'),
					r = a - buffer.source;
				UMK.setFlag('s', r < 0 ? 1 : 0);
				UMK.setFlag('z', r == 0 ? 1 : 0);
				UMK.setFlag('c', 0);
				if(r < 0) r += 256;
				UMK.setFlag('p', countBits(r) % 2 == 0 ? 1 : 0);
				UMK.setRegister('a', r);
				buffer.op++;
			}
		}
	];

	for(var i=0; i<opcodes.length; i++) {
		var mask = opcodes[i].template.replace(/[0-1]/g, "1").replace(/[A-Z]/g, "0"),
			sample = opcodes[i].template.replace(/[A-Z]/g, "0");
		opcodes[i].mask = parseInt(mask, 2);
		opcodes[i].sample = parseInt(sample, 2);
		opcodes[i].specifity = opcodes[i].template.replace(/[A-Z]/g, "").length;

		// extract masks for arguments
		mask = opcodes[i].template.indexOf('SSS');
		opcodes[i].source = mask > -1 ? opcodes[i].template.length - (mask + 3) : -1;
		mask = opcodes[i].template.indexOf('DDD');
		opcodes[i].dest = mask > -1 ? opcodes[i].template.length - (mask + 3) : -1;
		mask = opcodes[i].template.indexOf('CCC');
		opcodes[i].cond = mask > -1 ? opcodes[i].template.length - (mask + 3) : -1;
	}

	opcodes = opcodes.sort(function(a, b) {
		return a.specifity < b.specifity;
	});

	function processOpCode(opcode) {
		// detect opcode
		var temp = 0;
		for(var i=0; i<opcodes.length; i++) {	
			temp = opcode & opcodes[i].mask;
			if(temp == opcodes[i].sample) {
				// extract arguments
				if(opcodes[i].source > -1) {
					temp = opcode >> opcodes[i].source;
					buffer.source = temp & 7; // 7 = 0000 0111
				}
				if(opcodes[i].dest > -1) {
					temp = opcode >> opcodes[i].dest;
					buffer.dest = temp & 7; // 7 = 0000 0111
				}
				if(opcodes[i].cond > -1) {
					temp = opcode >> opcodes[i].cond;
					buffer.cond = temp & 7; // 7 = 0000 0111
				}
				console.log(opcodes[i].name);
				opcodes[i].operate();
				return 1;
			}
		}
		console.log("NOP");
		buffer.op++;
		return 0;
	}

})();