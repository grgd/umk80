
function Sensor(value, port, aug) {
	this.value = value || 0;
	this.port = port || 0;
	for(var i in aug)
		this[i] = aug[i];
}

/* TEMP */
var init = {
	in: false,
	out: false,
	heat: false,
	t: 23,
	power: 50.0,
	v: 0.0
};

function updateInit() {
	var h = window.location.hash.substr(1);
	if(h.length < 1)
		return;
	var a = h.split("&");
	for(var i=0; i<a.length; i++) {
		var z = a[i].split('='),
			v;
		if(z[0] == "t" || z[0] == "power" || z[0] == "v")
			v = parseFloat(z[1]);
		if(z[0] == "in" || z[0] == "out" || z[0] == "heat")
			v = z[1] == "true";
		init[z[0]] = v;
	}
}

var model = (function() {
	var INITIAL_T = 23.0,
		HEATER_POWER = 5.0,
		VALVE_FLOW = 0.01,
		HEATER_DURABILITY = 10; // количество тиков, после которых нагреватель сгорает

	var valveIn = new Sensor(true, 1, {flow: VALVE_FLOW}),
		valveOut = new Sensor(false, 2, {flow: VALVE_FLOW}),
		sensorTop = new Sensor(false, 3, {height: 0.9}),
		sensorBottom = new Sensor(false, 4, {height: 0.2}),
		heater = new Sensor(false, 5, {overheat: 0, power: HEATER_POWER}), // Celcius degrees
		thermo = new Sensor(INITIAL_T, 6);

	var waterLevel = 0.0,
		waterTemp = INITIAL_T,
		ticks = 0;

	var errors = {
		flood: false,
		heaterBurned: false
	}

	function reset() {
		updateInit();
		valveIn.value = init.in;//false;
		valveOut.value = init.out;//false;
		waterLevel = init.v;//0.0;
		waterTemp = init.t;//INITIAL_T;
		sensorTop.value = false;
		sensorBottom.value = false;
		heater.value = init.heat;//false;
		heater.power = init.power;
		heater.overheat = 0;
		thermo.value = waterTemp//INITIAL_T;
		ticks = 0;
		errors.flood = false;
		errors.heaterBurned = false;
	}

	function tick() {
		if(valveOut.value && waterLevel > 0.0)
			waterLevel -= valveOut.flow;

		if(valveIn.value) {
			waterLevel += valveIn.flow;
			waterTemp  = (INITIAL_T * valveIn.flow + waterTemp * waterLevel) / (waterLevel + valveIn.flow); // доливка холодной воды охлаждает на чуть-чуть
		}

		sensorBottom.value = waterLevel > sensorBottom.height;
		sensorTop.value = waterLevel > sensorTop.height;

		if(heater.value) {
			waterTemp += heater.power * 0.001 / Math.max(waterLevel, 0.001);
		} else if(heater.overheat > 0) {
			heater.overheat--;
		}

		if(waterTemp > 100.0) {
			heater.overheat ++;
			waterTemp = 100.0;
		}

		thermo.value = waterTemp;

		if(heater.overheat == HEATER_DURABILITY) {
			errors.heaterBurned = true;
		}

		if(waterLevel < 0.0)
			waterLevel = 0.0;

		if(waterLevel > 1.0) {
			waterLevel = 1.0;
			errors.flood = true;
		}

		ticks ++;
	}

	function getLevel() {
		return waterLevel;
	}

	function getTemp() {
		return waterTemp;
	}

	function getTicks() {
		return ticks;
	}

	function getErrors() {
		return errors;
	}

	return {
		valveIn: valveIn,
		valveOut: valveOut,
		sensorTop: sensorTop,
		sensorBottom: sensorBottom,
		heater: heater,
		thermo: thermo,
		waterLevel: getLevel,
		waterTemp: getTemp,
		errors: errors,
		tick: tick,
		ticks: getTicks,
		reset: reset
	}
})();

var modelView = (function () {
	var canvas = $('#model-canvas'),
		width,
		height;

	var running = false;

	var sprites = {
		bg: null,
		fg: null
	};

	$(window).resize(function() {
		width = canvas.width();
		height = canvas.height();
		repaint(true);
	});

	sprites.bg = loadImage("img/bg.jpg");
	sprites.fg = loadImage("img/fg.png");

	function loadImage(url) {
		var img = new Image();
		img.src = url;
		return img;
	}

	function repaint(full) {
		model.tick();

		var c = canvas[0].getContext("2d");
		c.drawImage(sprites.bg, 0, 0);

		c.fillStyle = "#005eae";

		if(model.valveIn.value)
			c.fillRect(502, 177, 15, 521);

		c.fillRect(422, 683 - model.waterLevel() * 457, 235, model.waterLevel() * 457);

		c.drawImage(sprites.fg, 330, 132);

		c.font = "35px monospace";
		c.fillStyle = "#5a5";
		c.fillText(model.valveIn.value ? "on" : "off", 344, 45);
		c.fillText(model.valveOut.value ? "on" : "off", 730, 552);
		c.fillText(model.sensorBottom.value ? "1" : "0", 332, 472);
		c.fillText(model.sensorTop.value ? "1" : "0", 332, 240);
		c.fillText(model.thermo.value | 0, 296, 565);

		if(model.errors.flood || model.errors.heaterBurned) {
			c.fillStyle = "#F33";
			c.globalAlpha = 0.1 - Math.cos(model.ticks() * 0.5) * 0.05;
			c.fillRect(0, 0, 1024, 768);
			c.globalAlpha = 1.0;
		}
	}

	var mainTimer = 0;

	function start() {
		mainTimer = setInterval(function() {
			repaint(false);
		}, 30);
		running = true;
	}

	function stop() {
		clearInterval(mainTimer);
		running = false;
	}

	function getRunning() {
		return running;
	}

	function reset() {
		model.reset();
		stop();
	}

	return {
		start: start,
		stop: stop,
		reset: reset,
		running: getRunning,
		repaint: repaint
	}
})();